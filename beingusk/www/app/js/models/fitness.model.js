/**
 * Created by UdaySravanK on 3/26/17.
 */
define([
    'jquery',
    'backbone',
    'js/models/common/base.model'
], function ($, Backbone, BaseModel) {
    var fitnessModel = BaseModel.extend({
        defaults: {
            welcomeText: 'Hey there!!'
        },
        tplData: {
            title: 'fitness page'
        },
        initialize: function () {
        }
    });

    return fitnessModel;
});